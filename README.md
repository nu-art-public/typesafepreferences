# README #

Just clone the project and import it into your Android Studio.

### What is this repository for? ###

* Some long lost thought I had about Generic type safe storage which fits perfectly as a wrapper around Android's SharedPreferences.

### Have some thoughts? ###

* You found a mistake, please open a ticket...
* You have a better way to do something, please open an ticket...
* Just feel like talking about it... [+AdamVanDerKruk](https://plus.google.com/+AdamZehavi/about)