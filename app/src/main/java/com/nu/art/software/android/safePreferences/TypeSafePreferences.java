package com.nu.art.software.android.safePreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;

public class TypeSafePreferences {

	private static final String EXPIRES_POSTFIX = "-Expires";
	private static final Gson gson = new Gson();

	public abstract class PreferenceType<Type> {

		private final String key;
		private final SharedPreferencesType type;
		private final Type defaultValue;
		private final long expires;

		public PreferenceType(String key, Type defaultValue) {
			this(key, defaultValue, -1);
		}

		public PreferenceType(String key, Type defaultValue, SharedPreferencesType type) {
			this(key, defaultValue, -1, type);
		}

		public PreferenceType(String key, Type defaultValue, long expires) {
			this(key, defaultValue, expires, null);
		}

		public PreferenceType(String key, Type defaultValue, long expires, SharedPreferencesType type) {
			this.key = key;
			this.type = type;
			this.defaultValue = defaultValue;
			this.expires = expires;
		}

		public final Type get() {
			SharedPreferences preferences = getPreferences(type);
			Type retValue = defaultValue;
			if (expires == -1)
				retValue = _get(preferences, key, defaultValue);
			else {
				long lastUpdated = preferences.getLong(key + EXPIRES_POSTFIX, -1);
				if (System.currentTimeMillis() - lastUpdated < expires) {
					retValue = _get(preferences, key, defaultValue);
				}
			}
			Log.d(TAG, "+----+ Fetch: " + key + ": " + retValue);
			return retValue;
		}

		protected abstract Type _get(SharedPreferences preferences, String key, Type defaultValue);

		public void set(Type value) {
			Editor editor = getPreferences(type).edit();
			Log.d(TAG, "+----+ Store: " + key + ": " + value);

			_set(editor, key, value);
			if (expires != -1)
				editor.putLong(key + EXPIRES_POSTFIX, System.currentTimeMillis());
			editor.apply();
		}

		protected abstract void _set(Editor preferences, String key, Type value);

		public final void clear() {
			clearExpiration(this);
		}

		public final void delete() {
			removeValue(this);
		}
	}

	public interface SharedPreferencesType {

		String getPreferencesName();

		int getMode();
	}

	private final SharedPreferencesType DefaultPreferences;
	private HashMap<SharedPreferencesType, SharedPreferences> preferencesMap = new HashMap<SharedPreferencesType, SharedPreferences>();
	private final String TAG = getClass().getSimpleName();
	private final Context context;

	protected TypeSafePreferences(Context c, SharedPreferencesType DefaultPreferences) {
		this.context = c;
		this.DefaultPreferences = DefaultPreferences;
	}

	public final void clearExpiration(PreferenceType<?> type) {
		Editor editor = getPreferences(type.type).edit();
		editor.putLong(type.key + EXPIRES_POSTFIX, -1L);
		editor.apply();
	}

	public final void removeValue(PreferenceType<?> type) {
		Editor editor = getPreferences(type.type).edit();
		editor.remove(type.key);
		editor.apply();
	}

	private SharedPreferences getPreferences(SharedPreferencesType type) {
		if (type == null)
			type = DefaultPreferences;

		SharedPreferences sharedPreferences = preferencesMap.get(type);
		if (sharedPreferences == null) {
			sharedPreferences = context.getSharedPreferences(type.getPreferencesName(), type.getMode());
			preferencesMap.put(type, sharedPreferences);
		}
		return sharedPreferences;
	}

	public void clearCache() {
		for (SharedPreferencesType key : preferencesMap.keySet()) {
			dropPreferences(key);
		}
	}

	public void dropPreferences(SharedPreferencesType type) {
		getPreferences(type).edit().clear().apply();
	}

	public final class PreferenceEnum<EnumType extends Enum<EnumType>> {

		private final StringPreference key;
		private final Class<EnumType> enumType;

		public PreferenceEnum(String key, Class<EnumType> enumType, EnumType defaultValue) {
			this(key, enumType, defaultValue, null);
		}

		public PreferenceEnum(String key, Class<EnumType> enumType, EnumType defaultValue, long expires) {
			this(key, enumType, defaultValue, expires, null);
		}

		public PreferenceEnum(String key, Class<EnumType> enumType, EnumType defaultValue, SharedPreferencesType type) {
			this(key, enumType, defaultValue, -1, type);
		}

		@SuppressWarnings("unchecked")
		public PreferenceEnum(String key, Class<EnumType> enumType, EnumType defaultValue, long expires, SharedPreferencesType type) {
			this.key = new StringPreference(key, defaultValue == null ? null : defaultValue.name(), expires, type);
			this.enumType = enumType;
		}

		public EnumType get() {
			String value = key.get();
			if (value == null)
				return null;
			return Enum.valueOf(enumType, value);
		}

		public void set(EnumType value) {
			key.set(value.name());
		}

		public void clear() {
			clearExpiration(key);
		}
	}

	public final class PreferenceJson<Type> {

		private final StringPreference key;
		private final Class<Type> itemType;

		public PreferenceJson(String key, Class<Type> type) {
			this(key, type, null);
		}

		public PreferenceJson(String key, Class<Type> type, long expires) {
			this(key, type, null, expires, null);
		}

		public PreferenceJson(String key, Class<Type> type, SharedPreferencesType preferencesType) {
			this(key, type, null, -1, preferencesType);
		}

		public PreferenceJson(String key, Type defaultValue) {
			this(key, defaultValue, null);
		}

		public PreferenceJson(String key, Type defaultValue, long expires) {
			this(key, defaultValue, expires, null);
		}

		public PreferenceJson(String key, Type defaultValue, SharedPreferencesType preferencesType) {
			this(key, defaultValue, -1, preferencesType);
		}

		@SuppressWarnings("unchecked")
		public PreferenceJson(String key, Type defaultValue, long expires, SharedPreferencesType preferencesType) {
			this(key, (Class<Type>) defaultValue.getClass(), defaultValue, expires, preferencesType);
		}

		@SuppressWarnings("unchecked")
		public PreferenceJson(String key, Class<Type> type, Type defaultValue, long expires, SharedPreferencesType preferencesType) {
			String defaultValueAsString = defaultValue == null ? null : gson.toJson(defaultValue);
			this.key = new StringPreference(key, defaultValueAsString, expires, preferencesType);
			itemType = type;
		}

		public Type get() {
			String value = key.get();
			return gson.fromJson(value, itemType);
		}

		public void set(Type value) {
			String valueAsString = value == null ? null : gson.toJson(value);
			key.set(valueAsString);
		}

		public void clear() {
			clearExpiration(key);
		}
	}

	public final class IntegerPreference
			extends PreferenceType<Integer> {

		public IntegerPreference(String key, Integer defaultValue) {
			super(key, defaultValue);
		}

		public IntegerPreference(String key, Integer defaultValue, SharedPreferencesType type) {
			super(key, defaultValue, type);
		}

		public IntegerPreference(String key, Integer defaultValue, long expires) {
			super(key, defaultValue, expires);
		}

		public IntegerPreference(String key, Integer defaultValue, long expires, SharedPreferencesType type) {
			super(key, defaultValue, expires, type);
		}

		@Override
		protected Integer _get(SharedPreferences preferences, String key, Integer defaultValue) {
			return preferences.getInt(key, defaultValue);
		}

		@Override
		protected void _set(Editor preferences, String key, Integer value) {
			preferences.putInt(key, value);
		}
	}

	public final class BooleanPreference
			extends PreferenceType<Boolean> {

		public BooleanPreference(String key, Boolean defaultValue) {
			super(key, defaultValue);
		}

		public BooleanPreference(String key, Boolean defaultValue, SharedPreferencesType type) {
			super(key, defaultValue, type);
		}

		public BooleanPreference(String key, Boolean defaultValue, long expires) {
			super(key, defaultValue, expires);
		}

		public BooleanPreference(String key, Boolean defaultValue, long expires, SharedPreferencesType type) {
			super(key, defaultValue, expires, type);
		}

		@Override
		protected Boolean _get(SharedPreferences preferences, String key, Boolean defaultValue) {
			return preferences.getBoolean(key, defaultValue);
		}

		@Override
		protected void _set(Editor preferences, String key, Boolean value) {
			preferences.putBoolean(key, value);
		}
	}

	public final class LongPreference
			extends PreferenceType<Long> {

		public LongPreference(String key, Long defaultValue) {
			super(key, defaultValue);
		}

		public LongPreference(String key, Long defaultValue, SharedPreferencesType type) {
			super(key, defaultValue, type);
		}

		public LongPreference(String key, Long defaultValue, long expires) {
			super(key, defaultValue, expires);
		}

		public LongPreference(String key, Long defaultValue, long expires, SharedPreferencesType type) {
			super(key, defaultValue, expires, type);
		}

		@Override
		protected Long _get(SharedPreferences preferences, String key, Long defaultValue) {
			return preferences.getLong(key, defaultValue);
		}

		@Override
		protected void _set(Editor preferences, String key, Long value) {
			preferences.putLong(key, value);
		}
	}

	public final class FloatPreference
			extends PreferenceType<Float> {

		public FloatPreference(String key, Float defaultValue) {
			super(key, defaultValue);
		}

		public FloatPreference(String key, Float defaultValue, SharedPreferencesType type) {
			super(key, defaultValue, type);
		}

		public FloatPreference(String key, Float defaultValue, long expires) {
			super(key, defaultValue, expires);
		}

		public FloatPreference(String key, Float defaultValue, long expires, SharedPreferencesType type) {
			super(key, defaultValue, expires, type);
		}

		@Override
		protected Float _get(SharedPreferences preferences, String key, Float defaultValue) {
			return preferences.getFloat(key, defaultValue);
		}

		@Override
		protected void _set(Editor preferences, String key, Float value) {
			preferences.putFloat(key, value);
		}
	}

	public final class StringPreference
			extends PreferenceType<String> {

		public StringPreference(String key, String defaultValue) {
			super(key, defaultValue);
		}

		public StringPreference(String key, String defaultValue, SharedPreferencesType type) {
			super(key, defaultValue, type);
		}

		public StringPreference(String key, String defaultValue, long expires) {
			super(key, defaultValue, expires);
		}

		public StringPreference(String key, String defaultValue, long expires, SharedPreferencesType type) {
			super(key, defaultValue, expires, type);
		}

		@Override
		protected String _get(SharedPreferences preferences, String key, String defaultValue) {
			return preferences.getString(key, defaultValue);
		}

		@Override
		protected void _set(Editor preferences, String key, String value) {
			preferences.putString(key, value);
		}
	}
}
