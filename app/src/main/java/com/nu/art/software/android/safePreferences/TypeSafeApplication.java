package com.nu.art.software.android.safePreferences;

import android.app.Application;

/**
 * Created by TacB0sS on 11-Jun 2015.
 */
public class TypeSafeApplication
		extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		MyAppPreferences.createInstance(this, PreferencesType.NotForBackup);
	}
}
