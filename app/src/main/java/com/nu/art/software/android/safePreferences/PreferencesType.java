package com.nu.art.software.android.safePreferences;

import com.nu.art.software.android.safePreferences.TypeSafePreferences.SharedPreferencesType;

public enum PreferencesType
		implements SharedPreferencesType {
	ForBackup("Backup", 0),
	NotForBackup("NotForBackup", 0),;

	private String preferencesName;
	private int mode;

	PreferencesType(String name, int mode) {
		this.preferencesName = name;
		this.mode = mode;
	}

	@Override
	public String getPreferencesName() {
		return preferencesName;
	}

	public int getMode() {
		return mode;
	}
}
