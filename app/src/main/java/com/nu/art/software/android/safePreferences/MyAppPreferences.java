package com.nu.art.software.android.safePreferences;

import android.content.Context;

/**
 * Created by TacB0sS on 11-Jun 2015.
 */
public class MyAppPreferences
		extends TypeSafePreferences {

	private static MyAppPreferences instance;

	public static void createInstance(Context c, SharedPreferencesType defaultPreferences) {
		instance = new MyAppPreferences(c, defaultPreferences);
	}

	public static MyAppPreferences getInstance() {
		return instance;
	}

	public final IntegerPreference storeInteger = new IntegerPreference("KEY_storeInteger", 0);
	public final BooleanPreference storeBoolean = new BooleanPreference("KEY_storeBoolean", false);
	public final StringPreference storeString = new StringPreference("KEY_storeString", "DefaultValue");

	protected MyAppPreferences(Context c, SharedPreferencesType DefaultPreferences) {
		super(c, DefaultPreferences);
	}
}
